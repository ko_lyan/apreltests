﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.Odbc;
using TestApplication.Model;
using TestApplication.Core;

namespace TestApplication
{

    public partial class FormGrid : Form
    {

        DataSetXls xls = new DataSetXls();
        DataProject FillXls = new DataProject();
        public FormGrid()
        {
            InitializeComponent();

            //Датасет экселя
            xls = new DataSetXls();


            //имя грида
            customDataGridView1.GUID = "xmlgrid";

            Grids grid = Helper.GetGridsByGUID(customDataGridView1.GUID);

            var colmns = Helper.GetListColumns(grid.ID);

            //новый   фильтр
            customDataGridView1.OnSettFilter += (like) =>
                {
                    FillXls.LikeText = like;
                    FillXls.GetDatasourse(xls);
                    drugBindingSource.DataSource = xls.Drug;
                };
            customDataGridView1.ColumnWidthChanged += ColumnWidthChanged;

            var heads = FillXls.GetHeadText();
            //заполнение колонок
            FillColumns(grid.ID  );
            //класс заполняет датасет
            FillXls.GetDatasourse(xls);

            drugBindingSource.DataSource = xls.Drug;
            customDataGridView1.DataSource = drugBindingSource;

 
        }
        //поменяли колонки местами
        private void customDataGridView1_ColumnDisplayIndexChanged(object sender, DataGridViewColumnEventArgs e)
        {

            DataGridViewColumn updcolumn = e.Column;
            string FieldName = updcolumn.DataPropertyName;

            Columns column = Helper.GetColumns(customDataGridView1 , FieldName);

            column.SortNumber = updcolumn.DisplayIndex;

            Helper.SubmitColumnBase(column);
        }
        //поменяли ширину
        void  ColumnWidthChanged(object sender,DataGridViewColumnEventArgs e)
        {
            DataGridViewColumn updcolumn = e.Column;
            string FieldName = updcolumn.DataPropertyName;

            Columns column = Helper.GetColumns(customDataGridView1, FieldName);

            column.Width = updcolumn.Width;
            //column.Show = selectedMenuItem.Checked;
            Helper.SubmitColumnBase(column);
        }

        //чекнули меню
        public void UncheckOtherToolStripMenuItems(ToolStripMenuItem selectedMenuItem)
        {
            selectedMenuItem.Checked = !selectedMenuItem.Checked;
            string FieldName = selectedMenuItem.Tag.ToString();

            VisibleColumn(FieldName, selectedMenuItem.Checked);

            selectedMenuItem.Owner.Show();
        }

        //событие смена видимости
        public void VisibleColumn(string FieldName, bool Checked)
        {


            Columns column = Helper.GetColumns(customDataGridView1, FieldName);

            column.Show = Checked;
            Helper.SubmitColumnBase(column);
            customDataGridView1.SetColumnsSetting(column);
        }

        //заполнение колонок и меню из склите и экссл
        void FillColumns(int GridID)
        {

            Grids grid = Helper.GetGridsByGUID(customDataGridView1.GUID);
            //можно брать из экселя
            var lstColumnsHead = FillXls.GetHeadText();

            foreach (DataColumn  c in xls.Drug.Columns)
            {

                //VisibleColumn
                var ct = lstColumnsHead.Where(x=>x.FieldName == c.ColumnName).FirstOrDefault();

                CustomDataGridViewTextBoxColumn ColumnGrid;
                ColumnGrid = new CustomDataGridViewTextBoxColumn();
                ColumnGrid.OnSwiched += VisibleColumn;
                ColumnGrid.HeaderText = (ct == null ? c.ColumnName : ct.HeadName);
                ColumnGrid.DataPropertyName = c.ColumnName;
                ColumnGrid.Name = c.ColumnName;
                ColumnGrid.CellTemplate = new DataGridViewTextBoxCell();
                customDataGridView1.Columns.Add(ColumnGrid);

            //menu
                ToolStripItem item = new ToolStripMenuItem();
                item.Text = ColumnGrid.HeaderText;
                item.Tag = c.ColumnName;
                item.Click += subietm1ToolStripMenuItem_Click;


                Columns colmns = Helper.GetColumn(grid.ID, c.ColumnName);

                ((ToolStripMenuItem)item).Checked = (colmns==null? true: colmns.Show );
                customDataGridView1._menu.Items.Add(item);
            }
            //вернул
            foreach (DataColumn c in xls.Drug.Columns)
            {
                Columns colmns = Helper.GetColumn(grid.ID, c.ColumnName);
                if (colmns != null)
                {
                    customDataGridView1.SetColumnsSetting(colmns);
                }
            }

             
        }
        


        private void subietm1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UncheckOtherToolStripMenuItems((ToolStripMenuItem)sender);
        }





 
        private void FormGrid_FormClosed(object sender, FormClosedEventArgs e)
        {


            Application.Exit();
        }

        private void btnCanSwitch_Click(object sender, EventArgs e)
        {

            foreach (var item in customDataGridView1._menu.Items)
            {
                ((ToolStripMenuItem)item).Checked = !((ToolStripMenuItem)item).Checked;
            }

            foreach (var col in customDataGridView1.Columns)
            {
                if (col is CustomDataGridViewTextBoxColumn)
                {
                    ((CustomDataGridViewTextBoxColumn)col).CanSwitch = !((CustomDataGridViewTextBoxColumn)col).CanSwitch;
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var ctrls = this.Controls
                .DataCheck();
        }


        bool DataCheck(Control c)
        {
            return c is ICheckableControl;
        }

        private void btnScreen_Click(object sender, EventArgs e)
        {
            ScreenCapture.Get();
        }

        //генерируем ошибку
        private void btnError_Click(object sender, EventArgs e)
        {
            string f = "aasss";
            object fObj = f;
           int a =  (int)fObj ;
        }

    }


}
