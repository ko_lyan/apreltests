﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TestApplication.Core;

namespace TestApplication
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                Application.Run(new FormAutorization());
            }
            catch (Exception ex)
            {
                ClientException.Process(ex);
            }
        }
    }
}
