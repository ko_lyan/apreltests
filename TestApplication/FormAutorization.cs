﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SQLite;
using TestApplication.Model;
using TestApplication.Core;
using System.IO;
namespace TestApplication
{
    public partial class FormAutorization : Form
    {
        public FormAutorization()
        {
            InitializeComponent();


            string ConStr =  @"Data Source=c:\1\db.db;Version=3;";
            SQLiteConnection connection = new SQLiteConnection(ConStr);
            connection.Open();

            ApplicationManager.GetInstance()._mdc = new sqliteDataContext(connection);
           
        }


        
        private void btnOk_Click(object sender, EventArgs e)
        {
            Autorization();
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                Autorization();
            }
        }

        void Autorization()
        {

            if (string.IsNullOrEmpty(txtName.Text) || string.IsNullOrEmpty(txtPass.Text) )
            {
                MessageBox.Show("Введите имя пользователя и пароль!!!");
                return;
            }


            Users user = Helper.ReturnUser(txtName.Text, txtPass.Text );
            if (user== null)
            {
                MessageBox.Show("Не найден пользователь!!!");
                return;
            }

            ApplicationManager.GetInstance()._currentUser = user;
            this.Visible = false;
            FormGrid f = new FormGrid();
            f.Show();
        }
    }
}
