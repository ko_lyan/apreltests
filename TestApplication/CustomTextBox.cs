﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestApplication.Core;

namespace TestApplication
{
    public partial class CustomTextBox : TextBox, ICheckableControl
    {
        public CustomTextBox()
        {
            EmptyDataCheck = true;
            EmptyDataText = "WARNING!!!";
            InitializeComponent();
        }
        public bool EmptyDataCheck { get; set; }

        public string EmptyDataText { get; set; }

        public bool Check()
        {
            bool RetCheck = string.IsNullOrEmpty(this.Text);
            if (EmptyDataCheck)
            {
                new BalloonTip().Show("title","Пустые данные",(Control)this,ToolTipIcon.Warning,500);
                //MessageBox.Show(EmptyDataText);
            }
            return RetCheck;
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
