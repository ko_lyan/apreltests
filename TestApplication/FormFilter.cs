﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestApplication
{
    public partial class FormFilter : Form
    {
        public string _Data ;
        public FormFilter(char k)
        {
            InitializeComponent();
            txtFilter.Text = k.ToString();
            txtFilter.SelectionStart = 1;
            txtFilter.SelectionLength = 0;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _Data = txtFilter.Text;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                _Data = txtFilter.Text;
                Close();
            }
        }
    }
}
