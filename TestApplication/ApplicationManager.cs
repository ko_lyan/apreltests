﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestApplication.Model;
using TestApplication.Core;

namespace TestApplication
{
    class ApplicationManager
    {

        public static ApplicationManager connectManager;

        
        public Users _currentUser;
        public sqliteDataContext _mdc;

        public string path = @"C:\1";
        private ApplicationManager()
        {

        }
        public static ApplicationManager GetInstance()
        {
            if (connectManager == null)
            {
                lock (typeof(ApplicationManager))
                {
                    if (connectManager == null)
                        connectManager = new ApplicationManager();
                }
            }
            return connectManager;
        }
    }
}
