﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication;
using TestApplication.Model ;

namespace TestApplication.Core
{
    public static class Helper
    {
        public static sqliteDataContext dc
        {
            get
            {
                return ApplicationManager.GetInstance()._mdc;
            }
        }

        //получает пользователя для авторизации
        public static Users ReturnUser(string Name, string Password)
        {

            var users = dc.Users.Where(x => x.User == Name && x.Password == Password).ToList();// );
            return users.FirstOrDefault();
        }

        //получает сущность склите на основании имени таблицы
        public static Grids GetGridsByGUID(string name)
        {

            var grids = dc.Grids.ToList().Where(x => x.GuiD == name && x.UserID == ApplicationManager.GetInstance()._currentUser.ID);
            if (grids.Any())
                return grids.First();
            else
            {
                Grids ret = new Grids
                {
                    UserID = ApplicationManager.GetInstance()._currentUser.ID,
                    GuiD = name
                };
                dc.Grids.InsertOnSubmit(ret);
                dc.SubmitChanges();
                return ret;
            }
        }

        //получает все колонки с настройками
        public static List<Columns> GetListColumns(int GridID)
        {
            List<Columns> ListColumns = new List<Columns>();

            var colmns = dc.Columns.Where(x => x.GunID == GridID).ToList();

            ListColumns.AddRange(colmns);

            return ListColumns;
        }
        //получает одну колонку
        public static Columns GetColumn(int GridID, string FieldName)
        {
            //без туЛист склите падает
            Columns column = dc.Columns.Where(x => x.GunID == GridID && x.FieldName == FieldName).ToList().FirstOrDefault();
            return column;
        }
        //не используется. Беру хедеры из экселя
        public static  List<TemplateTable> GetColumnsHeader(string GridName)
        {
            List<TemplateTable> lstColumnsHeader = new List<TemplateTable>();

            var columns = dc.TemplateTable.Where(x => x.GridName == GridName).ToList();

            lstColumnsHeader.AddRange(columns);

            return lstColumnsHeader;
        }

        //сохранение настроек в базу
        public static void SubmitColumnBase( Columns col )
        {
            Columns upd = dc.Columns.Where(x=>x.ID == col.ID).FirstOrDefault();

            if (upd != null)
            {
   
                upd.Width = col.Width;
                upd.Show = col.Show;
                upd.SortNumber = col.SortNumber;

                dc.SubmitChanges();
            }
            else
            {
                dc.Columns.InsertOnSubmit(col);
                dc.SubmitChanges();
            }
        }

        //получает настройки колонки текущй таблицы 
        public static Columns GetColumns(CustomDataGridView customDataGridView1, string FieldName)
        {
            Grids grid = Helper.GetGridsByGUID(customDataGridView1.GUID);
            var columns = Helper.GetListColumns(grid.ID);
            var column = columns.Where(x => x.FieldName == FieldName).FirstOrDefault();
            if (column == null)
            {
                column = customDataGridView1.GetColumn(FieldName);

            }
            return column;
        }
    }
}
