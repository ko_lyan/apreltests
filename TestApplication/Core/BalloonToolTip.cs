﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.ComponentModel;


namespace TestApplication.Core
{

    public class ShowToolBallon
    {


        [StructLayout(LayoutKind.Sequential)]
        public struct TOOLINFO
        {
            public int cbSize;
            public int uFlags;
            public IntPtr hwnd;
            public IntPtr id;
            public Rectangle rect;
            public IntPtr nullvalue;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string text;
            public uint param;

        }

        [StructLayout(LayoutKind.Sequential)]
        struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }


        [DllImport("user32.dll")]
        static extern IntPtr CreateWindowEx(int exstyle, string classname, string windowtitle,
            int style, int x, int y, int width, int height, IntPtr parent,
            int menu, int nullvalue, int nullptr);

        [DllImport("user32.dll")]
        static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        static extern int DestroyWindow(IntPtr hwnd);

        const int WS_POPUP = unchecked((int)0x80000000);
        const int TTS_BALLOON = 0x40;
        const int TTS_NOPREFIX = 0x02;
        const int TTS_ALWAYSTIP = 0x01;

        const int CW_USEDEFAULT = unchecked((int)0x80000000);

        const int WM_USER = 0x0400;

        IntPtr HWND_TOPMOST = new IntPtr(-1);

        const int SWP_NOSIZE = 0x0001;
        const int SWP_NOMOVE = 0x0002;
        const int SWP_NOACTIVATE = 0x0010;

        const int WS_EX_TOPMOST = 0x00000008;

        const int TTF_TRANSPARENT = 0x0100;
        const int TTF_TRACK = 0x0020;
        const int TTF_ABSOLUTE = 0x0080;



        static IntPtr _Tip;
        public static IntPtr GetTip(Control Owner)
        {

            if (_Tip == IntPtr.Zero)
            {
                _Tip = CreateWindowEx(0, "tooltips_class32", string.Empty, WS_POPUP | TTS_BALLOON | TTS_NOPREFIX | TTS_ALWAYSTIP,
                    CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
 IntPtr.Zero, 0, 0, 0);

            }
            return _Tip;
        }
        private const int TTDT_INITIAL = 3;
        private const int TTM_SETDELAYTIME = WM_USER + 3;
        const int TTM_UPDATETIPTEXT = WM_USER + 57;
        const int TTM_TRACKPOSITION = WM_USER + 18;
        const int TTM_SETTITLE = WM_USER + 33;
        const int TTM_TRACKACTIVATE = WM_USER + 17;
        const int TTM_ADDTOOL = WM_USER + 50;
        private const int TTDT_AUTOPOP = 2;

        public static void Start(Control Owner, Point pt, string title, string txt, ToolTipIcon icon, int timeMs)
        {
            TOOLINFO m_toolinfo = new TOOLINFO();
            m_toolinfo.text = txt;

            m_toolinfo.cbSize = Marshal.SizeOf(typeof(TOOLINFO));
            m_toolinfo.uFlags = TTF_TRANSPARENT | TTF_TRACK;
            m_toolinfo.hwnd = Owner.Handle;
            m_toolinfo.rect = Owner.Bounds;


            IntPtr m_wndToolTip = GetTip(Owner);



            SendMessage(m_wndToolTip, TTM_TRACKPOSITION, 0, makeLParam(pt));

            var tempptr = IntPtr.Zero;

            tempptr = Marshal.AllocHGlobal(m_toolinfo.cbSize);
            Marshal.StructureToPtr(m_toolinfo, tempptr, false);

            SendMessage(m_wndToolTip, TTM_UPDATETIPTEXT, 0, tempptr);
            int initial = 10000;
            SendMessage(_Tip, TTM_SETDELAYTIME, TTDT_AUTOPOP, new IntPtr(initial));//pizda ebanaya


            IntPtr tempptricon = Marshal.StringToHGlobalUni(title);
            SendMessage(m_wndToolTip, TTM_SETTITLE, (int)icon, tempptricon);

            var tempptrAddtool = IntPtr.Zero;
            tempptrAddtool = Marshal.AllocHGlobal(m_toolinfo.cbSize);
            Marshal.StructureToPtr(m_toolinfo, tempptrAddtool, false);

            SendMessage(m_wndToolTip, TTM_ADDTOOL, 0, tempptrAddtool);

            var tempptrs = IntPtr.Zero;

            tempptrs = Marshal.AllocHGlobal(m_toolinfo.cbSize);
            Marshal.StructureToPtr(m_toolinfo, tempptrs, false);

            SendMessage(m_wndToolTip, TTM_TRACKACTIVATE, Convert.ToInt32(true), tempptrs);

            //грохнуть эту ...
            System.Threading.Thread myThread = new System.Threading.Thread(() => DelWind(m_wndToolTip, tempptrs, timeMs)); 

            myThread.Start();



        }

        public static void DelWind(IntPtr h, IntPtr ti, int timeMs)
        {

            System.Threading.Thread.Sleep(timeMs);
            SendMessage(h, TTM_TRACKACTIVATE, Convert.ToInt32(false), ti);
        }


        internal static IntPtr makeLParam(Point pt)
        {
            int res = (pt.X & 0xFFFF);
            res |= ((pt.Y & 0xFFFF) << 16);
            return new IntPtr(res);
        }
    }
}
