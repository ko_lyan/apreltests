﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.Odbc;
using TestApplication.Model;

namespace TestApplication.Core
{
    public class DataProject
    {
        public string LikeText = " where 1=0";

        string mySelectQuery
        {
            get
            {
                return "SELECT top 10 * FROM [Лист 1$a4:k] " + LikeText;
            }
        }

        public void GetDatasourse( DataSetXls xls, string query=null)
        {

            xls.Clear();
            string fileNameString =string.Format(@"{0}\drugs.xls",ApplicationManager.GetInstance().path);

            string myConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileNameString + ";Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\"";
            // Define the database query. 

            // Create a database connection object using the connection string. 
            OleDbConnection myConnection = new OleDbConnection(myConnectionString);

            // Create a database command on the connection using query. 
            try
            {

                OleDbCommand myAccessCommand = new OleDbCommand( (query==null? mySelectQuery:query), myConnection);
                OleDbDataAdapter myDataAdapter = new OleDbDataAdapter(myAccessCommand);

                myConnection.Open();

                myDataAdapter.Fill(xls.Drug);

            }
            catch
            {
                return;
            }
            finally
            {
                myConnection.Close();
            }

        }

        public List<ColumnHead> GetHeadText()
        {
            List<ColumnHead> HeadCol = new List<ColumnHead>();
            string QueryHead = "SELECT top 1 * FROM [Лист 1$a3:k] ";
            DataSetXls xlshead = new DataSetXls();
            GetDatasourse(xlshead, QueryHead);
            var heads = xlshead.Drug.Columns.Cast<DataColumn>()
                .Select(x =>
                    new ColumnHead { FieldName = x.ColumnName, HeadName = xlshead.Drug.Rows[0].ItemArray[x.Ordinal].ToString().Trim() }
                    )
                .ToList();

            HeadCol.AddRange(heads);
            return HeadCol;
        }
    }
}
