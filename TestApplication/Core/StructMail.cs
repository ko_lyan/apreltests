﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApplication.Core
{
    public class StructMail
    {
        public string Message;
        public int Code;
        public string User;
        public string VersionOS;
        public string NetVersion;
    }
}
