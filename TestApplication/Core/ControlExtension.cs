﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestApplication.Core
{
    public static class ControlExtension
    {
        
        public static bool DataCheck(this Control.ControlCollection coll)
        {
            return coll.OfType<Control>()
                .Where(x => x is ICheckableControl)
                .Where(x => ((ICheckableControl)x).Check())
                .Any();
        }

    }
}
