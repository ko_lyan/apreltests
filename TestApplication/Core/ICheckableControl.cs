﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApplication.Core
{
//    EmptyDataCheck (boolean) и EmptyDataText (string). Добавить также метод Check(),
//который возвращает результат типа boolean.
    interface ICheckableControl
    {
         bool EmptyDataCheck { get; set; }
         string EmptyDataText { get; set; }
         bool Check();
    }
}
