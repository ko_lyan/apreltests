﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Globalization;
using System.Net.Mail;

namespace TestApplication.Core
{
    public static class ClientException
    {
        public static void Process(Exception ex)
        {
            StructMail SendMessage = new StructMail();

            SendMessage.Message = ex.Message;
            int Code = System.Runtime.InteropServices.Marshal.GetHRForException(ex);

            ScreenCapture.Get();

            SendMessage.VersionOS = Environment.OSVersion.ToString();
            SendMessage.User = Environment.UserName;
             
            RegistryKey installed_versions = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\NET Framework Setup\NDP");
            string[] version_names = installed_versions.GetSubKeyNames();

            double Framework = Convert.ToDouble(version_names[version_names.Length - 1].Remove(0, 1), CultureInfo.InvariantCulture);
            int SP = Convert.ToInt32(installed_versions.OpenSubKey(version_names[version_names.Length - 1]).GetValue("SP", 0));

            SendMessage.NetVersion = Framework.ToString() + (SP != 0 ? "SP" + SP : "");

            SendMessageError(SendMessage);


        }
        public static void SendMessageError(StructMail SM)
        {
            //мсдн
            MailMessage mail = new MailMessage("mikolasharit@gmail.com", "user@hotmail.com");
            SmtpClient client = new SmtpClient();
            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = "smtp.google.com";
            mail.Subject = "User_Error";
            mail.Body = string.Format("MessageError: {0}\nCode Error : {1}\nUser Name: {2} Operation System: {3} Framework Version = {4}", SM.Message, SM.Code , SM.User, SM.VersionOS, SM.NetVersion); ;
            string file = string.Format(@"{0}image.bmp", ApplicationManager.GetInstance().path);
            Attachment image_error = new Attachment(file, System.Net.Mime.MediaTypeNames.Application.Octet);
            mail.Attachments.Add(image_error);

            client.Send(mail);
        }

    }
}
