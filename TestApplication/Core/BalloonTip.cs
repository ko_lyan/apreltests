﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace TestApplication.Core
{
    struct TooltipInfo
    {
        public readonly string title;
        public readonly ToolTipIcon icon;
        public readonly string message;

        public TooltipInfo(string _title, ToolTipIcon _icon, string _message)
        {
            title = _title;
            icon = _icon;
            message = _message;
        }
    }


    public class BalloonTip
    {

        TooltipInfo tti;



        //https://msdn.microsoft.com/en-us/library/hh298368(v=vs.85).aspx
        public void Show(string title, string text, Control control, ToolTipIcon icon, int timeOut)
        {


            Point ptWhere = control.Parent.PointToScreen(control.Location);//this.PointToScreen(label1.Location); //this.PointToClient(label1.Location );
            ptWhere.X += (control.Width / 2);
            ptWhere.Y += (control.Height / 2);



            ShowToolBallon.Start(control, ptWhere, title, text, icon, timeOut);
        }
    }
}
