﻿namespace TestApplication
{
    partial class FormGrid
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCanSwitch = new System.Windows.Forms.Button();
            this.drugBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.btnScreen = new System.Windows.Forms.Button();
            this.btnError = new System.Windows.Forms.Button();
            this.customTextBox1 = new TestApplication.CustomTextBox();
            this.customDataGridView1 = new TestApplication.CustomDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.drugBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.customDataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCanSwitch
            // 
            this.btnCanSwitch.Location = new System.Drawing.Point(437, 26);
            this.btnCanSwitch.Name = "btnCanSwitch";
            this.btnCanSwitch.Size = new System.Drawing.Size(75, 23);
            this.btnCanSwitch.TabIndex = 1;
            this.btnCanSwitch.Text = "CanSwitch";
            this.btnCanSwitch.UseVisualStyleBackColor = true;
            this.btnCanSwitch.Click += new System.EventHandler(this.btnCanSwitch_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(299, 17);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 40);
            this.button1.TabIndex = 3;
            this.button1.Text = "Find in\r\nControlCollection";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnScreen
            // 
            this.btnScreen.Location = new System.Drawing.Point(545, 28);
            this.btnScreen.Name = "btnScreen";
            this.btnScreen.Size = new System.Drawing.Size(75, 23);
            this.btnScreen.TabIndex = 4;
            this.btnScreen.Text = "screen";
            this.btnScreen.UseVisualStyleBackColor = true;
            this.btnScreen.Click += new System.EventHandler(this.btnScreen_Click);
            // 
            // btnError
            // 
            this.btnError.Location = new System.Drawing.Point(636, 26);
            this.btnError.Name = "btnError";
            this.btnError.Size = new System.Drawing.Size(75, 23);
            this.btnError.TabIndex = 5;
            this.btnError.Text = "send error";
            this.btnError.UseVisualStyleBackColor = true;
            this.btnError.Click += new System.EventHandler(this.btnError_Click);
            // 
            // customTextBox1
            // 
            this.customTextBox1.EmptyDataCheck = true;
            this.customTextBox1.EmptyDataText = "WARNING!!!";
            this.customTextBox1.Location = new System.Drawing.Point(13, 28);
            this.customTextBox1.Name = "customTextBox1";
            this.customTextBox1.Size = new System.Drawing.Size(242, 20);
            this.customTextBox1.TabIndex = 2;
            // 
            // customDataGridView1
            // 
            this.customDataGridView1.AllowDrop = true;
            this.customDataGridView1.AllowUserToAddRows = false;
            this.customDataGridView1.AllowUserToDeleteRows = false;
            this.customDataGridView1.AllowUserToOrderColumns = true;
            this.customDataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.customDataGridView1.AutoGenerateColumns = false;
            this.customDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.customDataGridView1.CurrentColumn = null;
            this.customDataGridView1.DataSource = this.drugBindingSource;
            this.customDataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.customDataGridView1.GUID = null;
            this.customDataGridView1.Location = new System.Drawing.Point(0, 77);
            this.customDataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.customDataGridView1.Name = "customDataGridView1";
            this.customDataGridView1.RowTemplate.Height = 24;
            this.customDataGridView1.Size = new System.Drawing.Size(798, 340);
            this.customDataGridView1.TabIndex = 0;
            this.customDataGridView1.VirtualMode = true;
            this.customDataGridView1.ColumnDisplayIndexChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.customDataGridView1_ColumnDisplayIndexChanged);
            // 
            // FormGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 417);
            this.Controls.Add(this.btnError);
            this.Controls.Add(this.btnScreen);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.customTextBox1);
            this.Controls.Add(this.btnCanSwitch);
            this.Controls.Add(this.customDataGridView1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormGrid";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormGrid_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.drugBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.customDataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CustomDataGridView customDataGridView1;

        private System.Windows.Forms.BindingSource drugBindingSource;
        private System.Windows.Forms.Button btnCanSwitch;
        private CustomTextBox customTextBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnScreen;
        private System.Windows.Forms.Button btnError;

    }
}

