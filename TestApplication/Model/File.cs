// 
//  ____  _     __  __      _        _ 
// |  _ \| |__ |  \/  | ___| |_ __ _| |
// | | | | '_ \| |\/| |/ _ \ __/ _` | |
// | |_| | |_) | |  | |  __/ || (_| | |
// |____/|_.__/|_|  |_|\___|\__\__,_|_|
//
// Auto-generated from main on 2017-06-28 10:26:49Z.
// Please visit http://code.google.com/p/dblinq2007/ for more information.
//
namespace TestApplication.Model
{
	using System;
	using System.ComponentModel;
	using System.Data;
#if MONO_STRICT
	using System.Data.Linq;
#else   // MONO_STRICT
	using DbLinq.Data.Linq;
	using DbLinq.Vendor;
#endif  // MONO_STRICT
	using System.Data.Linq.Mapping;
	using System.Diagnostics;
	
	
	public partial class sqliteDataContext : DataContext
	{
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		#endregion
		
		
		public sqliteDataContext(string connectionString) : 
				base(connectionString)
		{
			this.OnCreated();
		}
		
		public sqliteDataContext(string connection, MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			this.OnCreated();
		}

        public sqliteDataContext(IDbConnection connection, MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			this.OnCreated();
		}
		
		public Table<Columns> Columns
		{
			get
			{
				return this.GetTable<Columns>();
			}
		}
		
		public Table<Grids> Grids
		{
			get
			{
				return this.GetTable<Grids>();
			}
		}
		
		public Table<TemplateTable> TemplateTable
		{
			get
			{
				return this.GetTable<TemplateTable>();
			}
		}
		
		public Table<Users> Users
		{
			get
			{
				return this.GetTable<Users>();
			}
		}
	}
	
	#region Start MONO_STRICT
#if MONO_STRICT

	public partial class sqliteDataContext
	{
		
		public sqliteDataContext(IDbConnection connection) : 
				base(connection)
		{
			this.OnCreated();
		}
	}
    #region End MONO_STRICT
    #endregion
#else     // MONO_STRICT

    public partial class sqliteDataContext
	{
		
		public sqliteDataContext(IDbConnection connection) : 
				base(connection, new DbLinq.Sqlite.SqliteVendor())
		{
			this.OnCreated();
		}
		
		public sqliteDataContext(IDbConnection connection, IVendor sqlDialect) : 
				base(connection, sqlDialect)
		{
			this.OnCreated();
		}

        public sqliteDataContext(IDbConnection connection, MappingSource mappingSource, IVendor sqlDialect) : 
				base(connection, mappingSource, sqlDialect)
		{
			this.OnCreated();
		}
	}
	#region End Not MONO_STRICT
	#endregion
#endif     // MONO_STRICT
	#endregion
	
	[Table(Name="main.Columns")]
	public partial class Columns : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _fieldName;
		
		private int _gunID;
		
		private System.Nullable<int> _id;
		
		private bool _show;
		
		private int _sortNumber;
		
		private int _width;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnFieldNameChanged();
		
		partial void OnFieldNameChanging(string value);
		
		partial void OnGunIDChanged();
		
		partial void OnGunIDChanging(int value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(System.Nullable<int> value);
		
		partial void OnShowChanged();
		
		partial void OnShowChanging(bool value);
		
		partial void OnSortNumberChanged();
		
		partial void OnSortNumberChanging(int value);
		
		partial void OnWidthChanged();
		
		partial void OnWidthChanging(int value);
		#endregion
		
		
		public Columns()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_fieldName", Name="fieldName", DbType="VARCHAR (50)", AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public string FieldName
		{
			get
			{
				return this._fieldName;
			}
			set
			{
				if (((_fieldName == value) 
							== false))
				{
					this.OnFieldNameChanging(value);
					this.SendPropertyChanging();
					this._fieldName = value;
					this.SendPropertyChanged("FieldName");
					this.OnFieldNameChanged();
				}
			}
		}
		
		[Column(Storage="_gunID", Name="gunID", DbType="INT", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int GunID
		{
			get
			{
				return this._gunID;
			}
			set
			{
				if ((_gunID != value))
				{
					this.OnGunIDChanging(value);
					this.SendPropertyChanging();
					this._gunID = value;
					this.SendPropertyChanged("GunID");
					this.OnGunIDChanged();
				}
			}
		}
		
		[Column(Storage="_id", Name="id", DbType="INTEGER", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public System.Nullable<int> ID
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((_id != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[Column(Storage="_show", Name="show", DbType="BOOLEAN", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public bool Show
		{
			get
			{
				return this._show;
			}
			set
			{
				if ((_show != value))
				{
					this.OnShowChanging(value);
					this.SendPropertyChanging();
					this._show = value;
					this.SendPropertyChanged("Show");
					this.OnShowChanged();
				}
			}
		}
		
		[Column(Storage="_sortNumber", Name="sortNumber", DbType="INT", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int SortNumber
		{
			get
			{
				return this._sortNumber;
			}
			set
			{
				if ((_sortNumber != value))
				{
					this.OnSortNumberChanging(value);
					this.SendPropertyChanging();
					this._sortNumber = value;
					this.SendPropertyChanged("SortNumber");
					this.OnSortNumberChanged();
				}
			}
		}
		
		[Column(Storage="_width", Name="width", DbType="INTEGER", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int Width
		{
			get
			{
				return this._width;
			}
			set
			{
				if ((_width != value))
				{
					this.OnWidthChanging(value);
					this.SendPropertyChanging();
					this._width = value;
					this.SendPropertyChanged("Width");
					this.OnWidthChanged();
				}
			}
		}
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="main.Grids")]
	public partial class Grids : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _guiD;
		
		private int _id;
		
		private System.Nullable<int> _userID;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnGuiDChanged();
		
		partial void OnGuiDChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(int value);
		
		partial void OnUserIDChanged();
		
		partial void OnUserIDChanging(System.Nullable<int> value);
		#endregion
		
		
		public Grids()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_guiD", Name="GUID", DbType="VARCHAR (50)", AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public string GuiD
		{
			get
			{
				return this._guiD;
			}
			set
			{
				if (((_guiD == value) 
							== false))
				{
					this.OnGuiDChanging(value);
					this.SendPropertyChanging();
					this._guiD = value;
					this.SendPropertyChanged("GuiD");
					this.OnGuiDChanged();
				}
			}
		}
		
		[Column(Storage="_id", Name="id", DbType="INTEGER", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public int ID
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((_id != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[Column(Storage="_userID", Name="UserID", DbType="INT", AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public System.Nullable<int> UserID
		{
			get
			{
				return this._userID;
			}
			set
			{
				if ((_userID != value))
				{
					this.OnUserIDChanging(value);
					this.SendPropertyChanging();
					this._userID = value;
					this.SendPropertyChanged("UserID");
					this.OnUserIDChanged();
				}
			}
		}
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="main.TemplateTable")]
	public partial class TemplateTable : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private string _fieldName;
		
		private string _gridName;
		
		private System.Nullable<int> _id;
		
		private string _nameColumn;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnFieldNameChanged();
		
		partial void OnFieldNameChanging(string value);
		
		partial void OnGridNameChanged();
		
		partial void OnGridNameChanging(string value);
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(System.Nullable<int> value);
		
		partial void OnNameColumnChanged();
		
		partial void OnNameColumnChanging(string value);
		#endregion
		
		
		public TemplateTable()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_fieldName", Name="FieldName", DbType="VARCHAR (50)", AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public string FieldName
		{
			get
			{
				return this._fieldName;
			}
			set
			{
				if (((_fieldName == value) 
							== false))
				{
					this.OnFieldNameChanging(value);
					this.SendPropertyChanging();
					this._fieldName = value;
					this.SendPropertyChanged("FieldName");
					this.OnFieldNameChanged();
				}
			}
		}
		
		[Column(Storage="_gridName", Name="GridName", DbType="VARCHAR (50)", AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public string GridName
		{
			get
			{
				return this._gridName;
			}
			set
			{
				if (((_gridName == value) 
							== false))
				{
					this.OnGridNameChanging(value);
					this.SendPropertyChanging();
					this._gridName = value;
					this.SendPropertyChanged("GridName");
					this.OnGridNameChanged();
				}
			}
		}
		
		[Column(Storage="_id", Name="ID", DbType="INTEGER", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public System.Nullable<int> ID
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((_id != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[Column(Storage="_nameColumn", Name="NameColumn", DbType="VARCHAR (50)", AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public string NameColumn
		{
			get
			{
				return this._nameColumn;
			}
			set
			{
				if (((_nameColumn == value) 
							== false))
				{
					this.OnNameColumnChanging(value);
					this.SendPropertyChanging();
					this._nameColumn = value;
					this.SendPropertyChanged("NameColumn");
					this.OnNameColumnChanged();
				}
			}
		}
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[Table(Name="main.users")]
	public partial class Users : System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
	{
		
		private static System.ComponentModel.PropertyChangingEventArgs emptyChangingEventArgs = new System.ComponentModel.PropertyChangingEventArgs("");
		
		private System.Nullable<int> _id;
		
		private string _password;
		
		private string _user;
		
		#region Extensibility Method Declarations
		partial void OnCreated();
		
		partial void OnIDChanged();
		
		partial void OnIDChanging(System.Nullable<int> value);
		
		partial void OnPasswordChanged();
		
		partial void OnPasswordChanging(string value);
		
		partial void OnUserChanged();
		
		partial void OnUserChanging(string value);
		#endregion
		
		
		public Users()
		{
			this.OnCreated();
		}
		
		[Column(Storage="_id", Name="id", DbType="INTEGER", IsPrimaryKey=true, IsDbGenerated=true, AutoSync=AutoSync.Never)]
		[DebuggerNonUserCode()]
		public System.Nullable<int> ID
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((_id != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._id = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[Column(Storage="_password", Name="password", DbType="VARCHAR (50)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string Password
		{
			get
			{
				return this._password;
			}
			set
			{
				if (((_password == value) 
							== false))
				{
					this.OnPasswordChanging(value);
					this.SendPropertyChanging();
					this._password = value;
					this.SendPropertyChanged("Password");
					this.OnPasswordChanged();
				}
			}
		}
		
		[Column(Storage="_user", Name="user", DbType="VARCHAR (50)", AutoSync=AutoSync.Never, CanBeNull=false)]
		[DebuggerNonUserCode()]
		public string User
		{
			get
			{
				return this._user;
			}
			set
			{
				if (((_user == value) 
							== false))
				{
					this.OnUserChanging(value);
					this.SendPropertyChanging();
					this._user = value;
					this.SendPropertyChanged("User");
					this.OnUserChanged();
				}
			}
		}
		
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
		
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			System.ComponentModel.PropertyChangingEventHandler h = this.PropertyChanging;
			if ((h != null))
			{
				h(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler h = this.PropertyChanged;
			if ((h != null))
			{
				h(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
