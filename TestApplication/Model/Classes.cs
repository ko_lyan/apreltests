﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestApplication.Model
{
    public class ColumnHead
    {
        public string FieldName { get; set; }
        public string HeadName { get; set; }
    }
}
