﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestApplication
{
    public delegate void SetCanSwitch(string FieldName, bool Swiched);
    public class CustomDataGridViewTextBoxColumn : DataGridViewTextBoxColumn
    {
        public event SetCanSwitch OnSwiched;
        public bool CanSwitch {
            get
            {
                return this.Visible;
            }
            set
            {

                this.Visible = value;
                if (OnSwiched != null)
                {
                    OnSwiched(this.DataPropertyName, value);
                }

            }
        }
    }
}
