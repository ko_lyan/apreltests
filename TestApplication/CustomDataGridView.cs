﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestApplication.Model;
using TestApplication.Core;

namespace TestApplication
{
    public delegate void SetFilter(string Like);

    public partial class CustomDataGridView : DataGridView
    {
        public event SetFilter OnSettFilter;
        DataGridViewColumn _CurrentColumn;
        public DataGridViewColumn CurrentColumn
        {
            get
            {
                return _CurrentColumn;
            }
            set
            {
                _CurrentColumn = value;
                if (value == null)
                    ClearHead();
            }
        }

        public string LikeText;
        public string FieldText;
        
        public string GUID { get; set; }

        public CustomDataGridView()
        {
            InitializeComponent();
            this.VirtualMode = true;
            AllowUserToAddRows = false;
            AllowUserToDeleteRows = false;
            this.ColumnHeaderMouseClick += ClickHeader;
            this.KeyPress += new KeyPressEventHandler(CustomKeyPress); 

            this.EditingControlShowing += dataGridView1_EditingControlShowing;

           

                 
        }

        public ContextMenuStrip _menu = new ContextMenuStrip();

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;


            e.Control.KeyPress += new KeyPressEventHandler(CustomKeyPress);
            
        }


        private void CustomKeyPress(object sender, KeyPressEventArgs e)
        {
            if (_CurrentColumn == null)
                return;
            FormFilter ff = new FormFilter(e.KeyChar);
            ff.ShowDialog();
            LikeText= ff._Data;
            FieldText = _CurrentColumn.DataPropertyName;
            CurrentColumn = null;
            if (OnSettFilter != null)
                OnSettFilter(string.Format(" where {0} Like '%{1}%' ",FieldText , LikeText ));

            e.Handled = true;
        }
        public void ClearHead()
        {
            foreach (DataGridViewColumn col in this.Columns)
            {
                col.HeaderCell.Style.Font = this.Font;
            }
        }
        private void ClickHeader(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {

                DataGridViewColumn newColumn = this.Columns[e.ColumnIndex];

                _menu.Show(this, this.PointToClient(Cursor.Position));

            }
            else
            {
                this.Focus();
                //присвоение всем фонт значение по умолчанию
                ClearHead();
                //текущая колонка
                _CurrentColumn = this.Columns[e.ColumnIndex];

                DataGridViewColumn newColumn = this.Columns[e.ColumnIndex];

                //делаем шрифт жирным
                Font headFont = new Font(this.Font, FontStyle.Bold);

                _CurrentColumn.HeaderCell.Style.Font = headFont;
            }
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void CustomDataGridView_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
        public Columns GetColumn(string FieldName)
        {
            int Position = 0;
             
            foreach (DataGridViewColumn col in this.Columns)
            {
                if (col.DataPropertyName == FieldName)
                {
                    Columns c = new Columns
                    {
                        GunID = Helper.GetGridsByGUID(this.GUID).ID,
                        FieldName = FieldName,
                        SortNumber = Position,
                        Show = true,
                        Width = col.Width,
                       
                    };
                    return c; 
                }
                Position++;
            }

            return null;
        }

        public void SetColumnsSetting(Columns col)
        {
            foreach (DataGridViewColumn c in this.Columns)
            {
                if (c.DataPropertyName == col.FieldName)
                {
                    c.Width = col.Width;
                    c.Visible = col.Show;
                    c.DisplayIndex = col.SortNumber;
                }
            }
        }
    }
}
