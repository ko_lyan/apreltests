﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Net.Http;
using System.Threading;
using System.IO;
using HtmlAgilityPack ;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;

namespace ConsoleDownload
{
    class Program
    {   
        private static readonly HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            if (args.Length !=1)
            {
                System.Console.WriteLine("Введите аргументы!!!");
                System.Console.ReadKey();
                return ;
            }
            
            string patch = args[0];
            if (!System.IO.Directory.Exists(patch))
            {
                System.Console.WriteLine("Несуществующий каталог!!!");
                System.Console.ReadKey();
                return;
            }
            Thread th = new Thread(() => DownXls(patch)); //Создаем новый объект потока (Thread)
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
            th.Join();
            
        }

        static void DownXls(string patch)
        {
             DateTime oldDate = new DateTime(2000, 1, 1);
            DateTime newDate = DateTime.Now;

            // Difference in days, hours, and minutes.
            TimeSpan ts = newDate - oldDate;

            // Difference in days.
            int differenceInDays = ts.Days;
            //запускаем веб браузер
            WebBrowser wb = new WebBrowser();
            wb.ScrollBarsEnabled = false;
            wb.ScriptErrorsSuppressed = true;

            wb.Navigate("http://www.grls.rosminzdrav.ru/LimPriceArchive.aspx");
            while (wb.ReadyState != WebBrowserReadyState.Complete) { Application.DoEvents(); }
            string linksdownload;
            //цикл на уменьшение дней, если текущий ещ   не выложили
            while (true)
            {
                wb.Navigate("javascript:__doPostBack('ctl00$plate$ca','" + differenceInDays + "')");

                while (wb.ReadyState != WebBrowserReadyState.Complete) { Application.DoEvents(); }

                var rez = wb.Document.Body.OuterHtml;

                //использую парсёр для нахождения ссылки
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(rez);

                var linkedPages = htmlDoc.DocumentNode.Descendants("a");
                linksdownload = linkedPages
                                      .Where(x => x.InnerHtml.Contains("в формате zip"))
                                      .Select(a => a.GetAttributeValue("href", null))
                                      .FirstOrDefault();
                if (linksdownload != null)
                    break;
                else
                    differenceInDays--;
            }
            string zipurl = @"http://www.grls.rosminzdrav.ru/" + linksdownload;
            Console.WriteLine("Скачивается файл по ссылке: " +zipurl);
            string zipfile = patch+"\\1.zip";
            new WebClient().DownloadFile(zipurl, zipfile);

            var ListFileName = ExtractZipFile(zipfile, "", patch);
            foreach (var f in ListFileName)
            {
                Console.WriteLine(string.Format("Извлечён файл : {0}", f));
            }
            string ExtractFile = ListFileName.Where(x => x.Contains("xls")).First();
            System.IO.File.Move(ExtractFile, patch+"\\grugs.xls");
            Console.WriteLine(patch + "\\grugs.xls");
            System.IO.File.Delete(zipfile);
            Console.ReadKey();
        }

        public static List<string> ExtractZipFile(string archiveFilenameIn, string password, string outFolder)
        {
            List<string> ret = new List<string>();
            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(archiveFilenameIn);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;		// AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;			// Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];		// 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        ret.Add(fullZipToPath);
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
                return ret;
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }
            }
        }


    }
}
